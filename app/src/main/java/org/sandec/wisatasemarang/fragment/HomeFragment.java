package org.sandec.wisatasemarang.fragment;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.sandec.wisatasemarang.R;
import org.sandec.wisatasemarang.adapter.WisataAdapter;
import org.sandec.wisatasemarang.model.ListWisataModel;
import org.sandec.wisatasemarang.model.WisataModel;
import org.sandec.wisatasemarang.rest.ApiService;
import org.sandec.wisatasemarang.rest.RetrofitConfig;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    private RecyclerView recycler;
    ArrayList<WisataModel> listData;
    private static final String TAG = "HomeFragment";

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //dataset
        listData = new ArrayList<>();

        //data dummy
//        for (int i = 0; i < 10; i++) {
//            WisataModel data1 = new WisataModel();
//            data1.setNama("Lawang Sewu");
//            data1.setAlamat("Jalam tugu Muda");
//            data1.setGambar("http://seputarwisatasemarang.000webhostapp.com/api/data/slider_wisata/lawangsewu.jpg");
//            listData.add(data1);
//        }

        //data online
        ambilDataOnline();

        //setup recyclerview
        recycler = (RecyclerView) view.findViewById(R.id.recycler_view);
        recycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        recycler.setAdapter(new WisataAdapter(getActivity(), listData));
    }

    private void ambilDataOnline() {
        //jika langsung array
//        ApiService api = RetrofitConfig.getApiService();
//        Call<ArrayList<WisataModel>> call = api.ambilDataWisata();
//        call.enqueue(new Callback<ArrayList<WisataModel>>() {
//            @Override
//            public void onResponse(Call<ArrayList<WisataModel>> call, Response<ArrayList<WisataModel>> response) {
//
//                if (response.isSuccessful()){
//                    listData = response.body();
//                    for (int i=0 ; i<listData.size() ; i++){
//                        Log.d(TAG, "onResponse: "+listData.get(i).getNama());
//                    }
//
//                    recycler.setAdapter(new WisataAdapter(getActivity(), listData));
//                } else {
//                    Toast.makeText(getActivity(), "Response Not Successful", Toast.LENGTH_SHORT).show();
//                }
//
//            }
//            @Override
//            public void onFailure(Call<ArrayList<WisataModel>> call, Throwable t) {
//                Log.d(TAG, "onFailure: "+t.getMessage());
//                Toast.makeText(getActivity(), "Http Failure: "+ t.getMessage(), Toast.LENGTH_SHORT).show();
//            }
//        });

        //jika ada JsonObjectnya dulu
        final ProgressDialog progress = new ProgressDialog(getActivity());
        progress.setTitle("Loading");
        progress.setMessage("Mohon Bersabar");
        progress.show();

        ApiService api = RetrofitConfig.getApiService();
        Call<ListWisataModel> call = api.ambilDataWisata();
        call.enqueue(new Callback<ListWisataModel>() {
                         @Override
                         public void onResponse(Call<ListWisataModel> call, Response<ListWisataModel> response) {
                             progress.hide();
                             Log.d(TAG, "onResponseMessage: "+ response.body().getSuccess());
                             if (response.isSuccessful()) {

                                 if (response.body().getSuccess().toString().equals("true")){
                                     listData = response.body().getWisata();
                                     for (int i = 0; i < listData.size(); i++) {
                                         Log.d(TAG, "onResponse: " + listData.get(i).getNamaWisata());
                                     }

                                     recycler.setAdapter(new WisataAdapter(getActivity(), listData));
                                 }

                             } else {
                                 Toast.makeText(getActivity(), "Response Not Successful : "+ response.body().getMessage(), Toast.LENGTH_SHORT).show();
                             }
                         }

                         @Override
                         public void onFailure(Call<ListWisataModel> call, Throwable t) {
                             Toast.makeText(getActivity(), "Response Failure : "+t.getMessage(), Toast.LENGTH_SHORT).show();
                         }
                     }
        );
    }
}

package org.sandec.wisatasemarang.activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.sandec.wisatasemarang.helper.Konstanta;
import org.sandec.wisatasemarang.R;
import org.sandec.wisatasemarang.model.WisataModel;

import java.util.ArrayList;

public class DetailWisataActivity extends AppCompatActivity {

    ArrayList<WisataModel> listData = new ArrayList<>();
    private static final String TAG = "DetailWisataActivity";
    private ImageView ivDetailGambar;
    private TextView tvDetailDeskripsi;
    private TextView tvDetailAlamat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_wisata);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        initView();

        Bundle data = getIntent().getExtras();
        String idWisata = data.getString(Konstanta.DATA_ID);
        String namaWisata = data.getString(Konstanta.DATA_NAMA);
        String gambarWisata = data.getString(Konstanta.DATA_GAMBAR);
        String deskripsiWisata = data.getString(Konstanta.DATA_DESKRIPSI);
        String alamatWisata = data.getString(Konstanta.DATA_ALAMAT);
        String latWisata = data.getString(Konstanta.DATA_LAT);
        String longWisata = data.getString(Konstanta.DATA_LNG);

        getSupportActionBar().setTitle(namaWisata);
        tvDetailDeskripsi.setText(deskripsiWisata);
        tvDetailAlamat.setText(alamatWisata);
        Glide.with(this)
                .load("http://52.187.117.60/wisata_semarang/img/wisata/" + gambarWisata)
                .placeholder(R.drawable.no_image_found)
                .error(R.drawable.no_image_found)
                .into(ivDetailGambar);
    }

    private void initView() {
        ivDetailGambar = (ImageView) findViewById(R.id.iv_detail_gambar);
        tvDetailDeskripsi = (TextView) findViewById(R.id.tv_detail_deskripsi);
        tvDetailAlamat = (TextView) findViewById(R.id.tv_detail_alamat);
    }
}
